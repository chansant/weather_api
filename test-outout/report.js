$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/java/weather/api/features/weather.feature");
formatter.feature({
  "line": 1,
  "name": "Weather API scenarios",
  "description": "",
  "id": "weather-api-scenarios",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Display the state code from response based on longitude and latitude",
  "description": "",
  "id": "weather-api-scenarios;display-the-state-code-from-response-based-on-longitude-and-latitude",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "I have \u003clatitude\u003e as latitude",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I have \u003clongitude\u003e as longitude",
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "I submit the get request with latitude, longitude and api key",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I display the state code from the response",
  "keyword": "Then "
});
formatter.examples({
  "line": 9,
  "name": "",
  "description": "",
  "id": "weather-api-scenarios;display-the-state-code-from-response-based-on-longitude-and-latitude;",
  "rows": [
    {
      "cells": [
        "latitude",
        "longitude"
      ],
      "line": 10,
      "id": "weather-api-scenarios;display-the-state-code-from-response-based-on-longitude-and-latitude;;1"
    },
    {
      "cells": [
        "40.730610",
        "-73.935242"
      ],
      "line": 11,
      "id": "weather-api-scenarios;display-the-state-code-from-response-based-on-longitude-and-latitude;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 11,
  "name": "Display the state code from response based on longitude and latitude",
  "description": "",
  "id": "weather-api-scenarios;display-the-state-code-from-response-based-on-longitude-and-latitude;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "I have 40.730610 as latitude",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I have -73.935242 as longitude",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "I submit the get request with latitude, longitude and api key",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I display the state code from the response",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "40.730610",
      "offset": 7
    }
  ],
  "location": "WatherAPIStepDefintion.latitude(String)"
});
formatter.result({
  "duration": 234414201,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "-73.935242",
      "offset": 7
    }
  ],
  "location": "WatherAPIStepDefintion.longitude(String)"
});
formatter.result({
  "duration": 85500,
  "status": "passed"
});
formatter.match({
  "location": "WatherAPIStepDefintion.getResponeByLanLon()"
});
formatter.result({
  "duration": 4482700999,
  "status": "passed"
});
formatter.match({
  "location": "WatherAPIStepDefintion.displayStateCode()"
});
formatter.result({
  "duration": 567266299,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 13,
  "name": "Display the sunrise timestamp and weather based on postal code",
  "description": "",
  "id": "weather-api-scenarios;display-the-sunrise-timestamp-and-weather-based-on-postal-code",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 14,
  "name": "I have \"28546\" as postal code",
  "keyword": "Given "
});
formatter.step({
  "line": 15,
  "name": "I submit the get request with postal code and api key",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "I display the sunrise timestamp and weather for all the data entries",
  "keyword": "Then "
});
formatter.examples({
  "line": 18,
  "name": "",
  "description": "",
  "id": "weather-api-scenarios;display-the-sunrise-timestamp-and-weather-based-on-postal-code;",
  "rows": [
    {
      "cells": [
        "postalcode"
      ],
      "line": 19,
      "id": "weather-api-scenarios;display-the-sunrise-timestamp-and-weather-based-on-postal-code;;1"
    },
    {
      "cells": [
        "28546"
      ],
      "line": 20,
      "id": "weather-api-scenarios;display-the-sunrise-timestamp-and-weather-based-on-postal-code;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 20,
  "name": "Display the sunrise timestamp and weather based on postal code",
  "description": "",
  "id": "weather-api-scenarios;display-the-sunrise-timestamp-and-weather-based-on-postal-code;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 14,
  "name": "I have \"28546\" as postal code",
  "keyword": "Given "
});
formatter.step({
  "line": 15,
  "name": "I submit the get request with postal code and api key",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "I display the sunrise timestamp and weather for all the data entries",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "\"28546\"",
      "offset": 7
    }
  ],
  "location": "WatherAPIStepDefintion.postalCode(String)"
});
formatter.result({
  "duration": 96901,
  "status": "passed"
});
formatter.match({
  "location": "WatherAPIStepDefintion.getResponeByPostalCode()"
});
formatter.result({
  "duration": 1392199000,
  "status": "passed"
});
formatter.match({
  "location": "WatherAPIStepDefintion.displayTSWeather()"
});
formatter.result({
  "duration": 492492100,
  "status": "passed"
});
});