Description:

This project is a small and simple example for getting the responses from Weather api
for various scenarios on different end points 

End points covered for Weather API:

1. Retrieve the response for the given latitude, longitude and api key - http://api.weatherbit.io/v2.0/current?lat=40.730610&lon=-73.935242&key=<your api key>
2. Retrieve the response for the given postal code and api key
https://api.weatherbit.io/v2.0/forecast/daily?postal_code=28546&key=<your api key>

Technology stack:

REST assured using Java, Cucumber, Log 4j, Maven and JUnit 

Usage:

Run using Maven command - mvn clean install 