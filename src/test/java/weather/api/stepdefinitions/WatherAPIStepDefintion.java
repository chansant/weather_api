package weather.api.stepdefinitions;

import static io.restassured.RestAssured.given;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import weather.api.util.Constants;
import weather.api.util.Utilities;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

/**
 * This class is used to glue the steps mentioned in the feature file
 * 
 * @author Chandra
 *
 */

public class WatherAPIStepDefintion {
	private static Logger log = LogManager.getLogger(WatherAPIStepDefintion.class.getName());
	private String latitude;
	private String longitude;
	private String postalCode;
	private JsonPath jp;

	// Assign the latitude value
	
	@Given("^I have (.+) as latitude$")
	public void latitude(String latitude) {
		this.latitude = latitude;
	}

	// Assign the longitude value
	
	@And("^I have (.+) as longitude$")
	public void longitude(String longitude) {
		this.longitude = longitude;
	}

	// Assign the postal code value
	
	@Given("^I have (.+) as postal code$")
	public void postalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	// Send the api call to retrieve the response for the given longitude, latitude and the api key

	@When("^I submit the get request with latitude, longitude and api key$")
	public void getResponeByLanLon() {

		RestAssured.baseURI = Constants.BASE_URI;
		log.info("Before calling the current weather API by longitude and latitude");
		Response resp = given().queryParam(Constants.WEATHER_LATITUDE_PARAM, latitude)
				.queryParam(Constants.WEATHER_LONGITUDE_PARAM, longitude)
				.queryParam(Constants.WEATHER_KEY, Constants.WEATHER_API_KEY_VALUE).when()
				.get(Constants.WEATHER_LAN_LON_RESOURCE_URI).then().extract().response();
		jp = Utilities.rawToJson(resp);
	}

	// Send the api call to retrieve the response for the given postal code and the api key
	
	@When("^I submit the get request with postal code and api key$")
	public void getResponeByPostalCode() {

		RestAssured.baseURI = Constants.BASE_URI;
		log.info("Before calling the 16 day daily forecast API");
		Response resp = given().queryParam(Constants.WEATHER_POSTAL_CODE_PARAM, postalCode)
				.queryParam(Constants.WEATHER_KEY, Constants.WEATHER_API_KEY_VALUE).when()
				.get(Constants.WEATHER_POSTAL_CODE_RESOURCE_URI).then().extract().response();
		jp = Utilities.rawToJson(resp);
	}

	// Display the state code in the log.out file
	
	@Then("^I display the state code from the response$")
	public void displayStateCode() {
		log.info("State code ---->   " + jp.get("data[0].state_code"));
		log.info("After displaying the state code from current weather API by longitude and latitude");
		log.info("***********************************************************************************");
	}
	
	// Display the sunrise timestamp and weather in the log.out file
	
	@Then("^I display the sunrise timestamp and weather for all the data entries$")
	public void displayTSWeather() {
		for (int i = 0; i < jp.getList("data").size(); i++) {
			Map weatherMap = (HashMap) jp.getList("data").get(i);
			log.info("sunrise_ts ----> " + Utilities.convertTimestamp(weatherMap.get("sunrise_ts").toString()));
			log.info("Weather --->     " + weatherMap.get("weather"));
		}
		log.info("After displaying the 16 day daily forecast API");
		log.info("***********************************************************************************");
	}
}