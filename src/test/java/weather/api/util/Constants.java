package weather.api.util;

/**
 * To define Weather api constants
 * @author Chandra
 *
 */

public class Constants {
	
	public static final String BASE_URI = "https://api.weatherbit.io/v2.0";
	public static final String WEATHER_LAN_LON_RESOURCE_URI = "/current";
	public static final String WEATHER_POSTAL_CODE_RESOURCE_URI = "/forecast/daily";
	public static final String WEATHER_POSTAL_CODE_PARAM = "postal_code";
	public static final String WEATHER_LATITUDE_PARAM = "lat";
	public static final String WEATHER_LONGITUDE_PARAM = "lon";
	public static final String WEATHER_KEY = "key";
	public static final String WEATHER_API_KEY_VALUE = "dfe569b804434895815b44bad15dd1f6";
	public static final String TIME_STAMP_PATTERN = "dd MMM yyyy HH:mm:ss";
	public static final Long MILLISECONDS = 1000L;
}
