package weather.api.util;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

/**
 * To define all the common methods
 * 
 * @author Chandra
 *
 */
public class Utilities {
	/**
	 * Convert Response object to JsonPath object
	 * 
	 * @param resp
	 * @return JsonPath object
	 */
	public static JsonPath rawToJson(Response resp) {
		String respString = resp.asString();
		JsonPath js = new JsonPath(respString);
		return js;
	}

	/**
	 * Convert unix timestamp to datetime
	 * 
	 * @param timestampAsString
	 * @return
	 */
	public static String convertTimestamp(String timestampAsString) {

		Date date = new java.util.Date(Long.valueOf(timestampAsString) * Constants.MILLISECONDS);
		SimpleDateFormat sdf = new java.text.SimpleDateFormat(Constants.TIME_STAMP_PATTERN);
		String formattedDate = sdf.format(date);
		return formattedDate;
	}

}
