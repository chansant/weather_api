Feature: Weather API scenarios

  Scenario Outline: Display the state code from response based on longitude and latitude
    Given I have <latitude> as latitude
    And I have <longitude> as longitude
    When I submit the get request with latitude, longitude and api key
    Then I display the state code from the response

    Examples: 
      | latitude  | longitude  |
      | 40.730610 | -73.935242 |

  Scenario Outline: Display the sunrise timestamp and weather based on postal code
    Given I have "28546" as postal code
    When I submit the get request with postal code and api key
    Then I display the sunrise timestamp and weather for all the data entries

    Examples: 
      | postalcode |
      |      28546 |
